package com;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Created by grkim on 2015-12-31.
 */
public class JdbcContext {

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void workWithStatementStrategy(StatementStrategy st) throws SQLException {

        Connection c = null;
        PreparedStatement ps;

        c = this.dataSource.getConnection();
        ps = st.makePreparedStatement(c);
        ps.executeUpdate();

        ps.close();
        c.close();

    }

}
