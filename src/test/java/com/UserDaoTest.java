package com;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static org.junit.Assert.assertEquals;

/**
 * Created by grkim on 2015-12-31.
 */
public class UserDaoTest {
    private ApplicationContext context;
    UserDao dao;
    private User user1;
    private User user2;
    private User user3;

    @Before
    public void setUp() throws Exception {
        this.context = new ClassPathXmlApplicationContext("applicationContext.xml");
        this.dao = context.getBean("userDao", UserDao.class);

        this.user1 = new User("oceanfog", "1123", "krk");
        this.user2 = new User("oceanfog2", "1123", "krk");
        this.user3 = new User("oceanfog3", "1123", "krk");
    }


    @Test
    public void testGet() throws Exception {
        User user = dao.get("oceanfog");
        assertEquals(user1.getId(), user.getId());
    }
}